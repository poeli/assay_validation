set -xe

./primer_validation_vis.py \
  -n test/SARS-CoV-2_all_alignment.fna.treefile.20200414 \
  -o dist/data/SARS-CoV-2.xml \
  -m test/Resources_gisaid_cov2020_metadata_all.json test/Resources_nextstrain_ncov_metadata.txt test/gisaid_cov2020_metadata_refseq.json \
  -p test/match_table.csv \
  -r test/Assay_Results.json

cp test/db_totals.json dist/data/
cp test/summary_table.json dist/data/

node phyd3.js &
open http://127.0.0.1:60500/
