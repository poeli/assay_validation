# Visualization for Assay Validation

## Requirements and Installation

```bash
# install python packages
conda install pandas
conda install -c conda-forge biopython bokeh
conda install -c bioconda ete3
# install PhyD3 for testing in local browser
npm install
node phyd3.js &
open http://127.0.0.1:8080/
```

## Usage

```bash
# remove old data
rm -rf dist/data/

# generate new data to dist/data/
./primer_validation_vis.py \
  -n SARS-CoV-2.treefile \
  -m metadata.tsv \
  -p match_table.csv \
  -a assay.txt \
  -r Assay_Results.json \
  -o dist/data/SARS-CoV-2.xml

cp db_totals.json dist/data/
cp summary_table.json dist/data/

# pack new data to data.tar.gz
cd dist
tar -czf data.tar.gz data
```
